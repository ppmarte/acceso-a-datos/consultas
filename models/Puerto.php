<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "puerto".
 *
 * @property string $nompuerto
 * @property int $altura
 * @property string $categoria
 * @property float|null $pendiente
 * @property int $numetapa
 * @property int|null $dorsal
 *
 * @property Ciclista $dorsal0
 * @property Etapa $numetapa0
 */
class Puerto extends \yii\db\ActiveRecord
{
    
    public $Total;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'puerto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nompuerto', 'altura', 'categoria', 'numetapa'], 'required'],
            [['altura', 'numetapa', 'dorsal'], 'integer'],
            [['pendiente'], 'number'],
            [['nompuerto'], 'string', 'max' => 35],
            [['categoria'], 'string', 'max' => 1],
            [['nompuerto'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::class, 'targetAttribute' => ['dorsal' => 'dorsal']],
            [['numetapa'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::class, 'targetAttribute' => ['numetapa' => 'numetapa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nompuerto' => 'Nompuerto',
            'altura' => 'Altura',
            'categoria' => 'Categoria',
            'pendiente' => 'Pendiente',
            'numetapa' => 'Numetapa',
            'dorsal' => 'Dorsal',
        ];
    }

    /**
     * Gets query for [[Dorsal0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Numetapa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumetapa0()
    {
        return $this->hasOne(Etapa::class, ['numetapa' => 'numetapa']);
    }
}

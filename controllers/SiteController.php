<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Etapa;
use app\models\Equipo;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
  /*INICIO DE LAS CONSULTAS*/
    
    //Consulta 1
 
    //Active Record
    public function actionConsulta1a(){
        //Se crea un provedor de datos
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()  -> select('count(*) as Total'),
        ]);
        //se renderiza la vista donde vamos a mostrar los datos
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado" => "Número de ciclistas que hay",
            "sql"=> "SELECT count(*) from ciclista",
        ]);
    }
    //DAO 
   public function actionConsulta1(){
        $dataProvider=new SqlDataProvider([
            'sql' => 'SELECT count(*) as Total from ciclista',  
         ]);
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) from ciclista",
            
        ]);   
   }
     
    //CONSULTA 2
   
   //CON ACTIVE RECORD
   public function actionConsulta2a(){
       $dataProvider= new ActiveDataProvider([
           'query' => Ciclista::find() ->select('count(*) as Total') ->where("nomequipo='Banesto'"),
       ]);
       
       return $this -> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=> ['Total' ],
           "titulo" => "Consuta 2 con Active Record",
           "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
           "sql"=>"Select count(*) FROM ciclista WHERE nomequipo='Banesto' ",
       ]);
   }
   //CON DAO
    public function actionConsulta2(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'Select count(*) as Total FROM ciclista WHERE nomequipo="Banesto"',
        ]);
        
        return $this -> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=> ['Total' ],
           "titulo" => "Consuta 2 con DAO",
           "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
           "sql"=>"Select count(*) FROM ciclista WHERE nomequipo='Banesto' ",
       ]);
    }
    
    //CONSULTA 3
    
    //Consulta con Active Record
    public function actionConsulta3a(){
        $dataProvider= new ActiveDataProvider([
            'query'=> Ciclista::find()->select('AVG(edad) as Media'),
        ]);
        
         return $this -> render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['Media'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=> "Edad media de los ciclistas",
            "sql"=>"Select AVG(edad) as Media from ciclista",
        ]);
    }
    
    //Consulta con DAO
    public function actionConsulta3(){
        $dataProvider = new SqlDataProvider([
            'sql' => 'Select AVG(edad) as Media from ciclista',
        ]);
        
        return $this -> render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['Media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=> "Edad media de los ciclistas",
            "sql"=>"Select AVG(edad) as Media from ciclista",
        ]);
    }
  //CONSULTA 4
    //Consulta con Active Record
    
    public function actionConsulta4a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista:: find()-> select('AVG(edad) as Media') -> where('nomequipo="Banesto"'),
        ]);
        return $this -> render('resultado',[
           "resultados"=>$dataProvider,
            "campos"=>['Media'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto' ",
        ]);
    }
    //Consulta con DAO
    public function actionConsulta4(){
        $dataProvider = new SqlDataProvider([
           'sql'=> 'Select AVG(edad) as Media from ciclista where nomequipo="Banesto" ', 
        ]);
        return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['Media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) as Media FROM ciclista WHERE nomequipo='Banesto' ",
        ]);
    }
    
    
    //CONSULTA 5
    //consulta con Active Record
    public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()-> select('AVG(edad) as Media, nomequipo') ->groupBy('nomequipo'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Media'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) as Media FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    //consulta con DAO
    public function actionConsulta5(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'Select nomequipo, AVG(edad) as Media FROM ciclista GROUP BY nomequipo', 
        ]);
        
          return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) as Media FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
//CONSULTA 6
    
    //Consulta con Active Record
     public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()-> select('count(*) as Total, nomequipo') ->groupBy('nomequipo'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta  6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) as Total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    //consulta con DAO
    public function actionConsulta6(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT nomequipo, COUNT(*) as Total FROM ciclista GROUP BY nomequipo', 
        ]);
        
             return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta  6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(*) as Total FROM ciclista GROUP BY nomequipo",
        ]);
        
    }
    

 //CONSULTA 7
    
    //Consulta con Active Record
     public function actionConsulta7a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()-> select('count(*) as Total') ,
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
        ]);
    }
    //consulta con DAO
        public function actionConsulta7(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT COUNT(*) as Total FROM puerto', 
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) FROM puerto",
        ]);
    }
    
    
    
//CONSULTA 8
    
    //Consulta con Active Record
     public function actionConsulta8a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()-> select('count(*) as Total') ->where('altura >1500'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura >1500",
        ]);
    }
    //consulta con DAO
    public function actionConsulta8(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT COUNT(*) as Total FROM puerto WHERE altura >1500', 
        ]);
        
             return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['Total'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura >1500",
        ]);
    }
    
   
//CONSULTA 9
    
    //Consulta con Active Record
     public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()-> select('nomequipo, count(*) as Total') ->groupBy('nomequipo') ->having('Count(*) > 4'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, count(*) as Total FROM ciclista GROUP BY nomequipo HAVING COUNT(*) >4",
        ]);
    }
    //consulta con DAO
    public function actionConsulta9(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT nomequipo, count(*) as Total FROM ciclista GROUP BY nomequipo HAVING COUNT(*) >4', 
        ]);
        
        return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, count(*) as Total FROM ciclista GROUP BY nomequipo HAVING COUNT(*) >4",
        ]);
    }
    
    
//CONSULTA 10
    
    //Consulta con Active Record
     public function actionConsulta10a(){
        $dataProvider = new ActiveDataProvider([
           'query'=>Ciclista::find()-> select('nomequipo, count(*) as Total') ->where('edad between 28 and 32') ->groupBy('nomequipo') ->having('Count(*) > 4'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo, COUNT(*) as Total FROM Ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    //consulta con DAO
        public function actionConsulta10(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT nomequipo, COUNT(*) as Total FROM Ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4', 
        ]);
        
          return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['nomequipo', 'Total'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo, COUNT(*) as Total FROM Ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
        
    }
    
    
//CONSULTA 11
    
    //Consulta con Active Record
     public function actionConsulta11a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Etapa::find()-> select('dorsal, count(*) as Total') ->groupBy('dorsal'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['dorsal', 'Total'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) as Total FROM etapa GROUP BY dorsal",
        ]);
    }
    //consulta con DAO
        public function actionConsulta11(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT dorsal, COUNT(*) as Total FROM etapa GROUP BY dorsal', 
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['dorsal', 'Total'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) as Total FROM etapa GROUP BY dorsal",
        ]);
        
    }
    
    
      //CONSULTA 12
    
    //Consulta con Active Record
     public function actionConsulta12a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Etapa::find()-> select('dorsal, count(*) as Total') -> groupBy('dorsal') -> having('Count(*) > 1'),
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['dorsal', 'Total'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, count(*) as Total FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1",
        ]);
    }
    //consulta con DAO
        public function actionConsulta12(){
        $dataProvider= new SqlDataProvider([
           'sql'=> 'SELECT dorsal, count(*) as Total FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1', 
        ]);
        
         return $this -> render('resultado',[
           "resultados"=> $dataProvider,
            "campos"=>['dorsal', 'Total'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal, count(*) as Total FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1",
        ]);
    }
    
      
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Lleva $model */

$this->title = 'Create Lleva';
$this->params['breadcrumbs'][] = ['label' => 'Llevas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lleva-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

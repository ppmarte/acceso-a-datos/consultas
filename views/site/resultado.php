<?php
use yii\grid\GridView;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    <P class="lead"><?= $enunciado ?></P>
    <div class="well">
        <?= $sql?>
    </div>
    </div>
    <?=    GridView::widget([
        'dataProvider'=>$resultados,
        'columns' => $campos
    ]);?>

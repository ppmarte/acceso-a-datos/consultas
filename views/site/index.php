<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de selección 2</h1>
    </div>
        
        
<div class="body-content">
         <!-- Inicio de la primera fila-->

        <div class="row">
            <!--Primera consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin primera consulta-->
            
            <!--Segunda consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay del equipo Banesto.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta2'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin segunda consulta-->
            
            <!--Tercera consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta3'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin Tercera consulta-->
        </div>
         
         <!-- Fin de la primera fila-->
         
         
          
         <!-- Inicio de la Segunda fila-->
         <div class="row">
             <!--Cuarta consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>La edad media de los del equipo Banesto.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta4'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin Cuarta consulta-->
            <!--Quinta consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>La edad media de los ciclistas por cada equipo.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta5'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin Quinta consulta-->
            <!--Sexta consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta6a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta6'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin sexta consulta--> 
            
        </div>
        <!-- Fin de la segunda fila--> 
         
        <!-- Inicio de la tercera fila-->
         <div class="row">
             <!--septima consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta7a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta7'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin septima consulta-->
            
            <!--octava consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta8'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin octava consulta-->
            <!--Novena consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta9a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta9'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin novena consulta--> 
            
        </div>
         <!-- Fin de la tercera fila--> 
         
          <!-- Inicio de la carta fila-->
         <div class="row">
             <!--decina consulta-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta10a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta10'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin decima consulta-->
            <!--consulta once-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta11a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta11'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin de la consulta once-->
            
            
             <!--consulta doce-->
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta12a'], ['class' => 'btn btn-primary'] ) ?>
                            <?= Html::a('DAO',['site/consulta12'], ['class' => 'btn btn-warning'] ) ?>

                        </p>
                    </div>
                </div>
                
                
            </div>
            <!-- Fin de la consulta doce-->
            
        </div>
         <!-- Fin de la cuarta fila--> 

    </div>
        

    
</div>
